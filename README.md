# JMH benchmark sample

Uses the [Java Microbenchmark Harness](http://openjdk.java.net/projects/code-tools/jmh/) (JMH)
for tests.

Tests taken from Java Magazine March/April 2015 ["The Quantum Physics of Java".](https://www.pdf-archive.com/2015/10/22/javamagazine20150304-dl/javamagazine20150304-dl.pdf#%5B%7B%22num%22%3A73%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22Fit%22%7D%5D)  
Compares running two loops over the array with 67,000 integer elements.

1st loop: changes every element  
2nd loop: changes every sixteenth element

### Building the benchmarks

    $ mvn clean install

### Running the benchmarks

    $ java -jar target/benchmarks.jar

### Measurements

####Intel® Core™ i7-2600 Processor (8M Cache, up to 3.80 GHz)
Benchmark             | Mode | Cnt  | Score  | Error   | Units
----------------------|------|------|--------|---------|------
MyBenchmark.testLoop1 | avgt | 25   | 30.257 | ± 0.457 | ms/op  
MyBenchmark.testLoop2 | avgt | 25   | 29.946 | ± 0.264 | ms/op


####Intel® Core™ i7-4770S Processor (8M Cache, up to 3.90 GHz)
Benchmark             | Mode | Cnt  | Score  | Error   | Units
----------------------|------|------|--------|---------|------
MyBenchmark.testLoop1 | avgt | 25   | 26.909 | ± 0.278 | ms/op  
MyBenchmark.testLoop2 | avgt | 25   | 26.021 | ± 0.297 | ms/op

### Who do I talk to? ###

[![](https://stackexchange.com/users/flair/4007082.png)](https://stackoverflow.com/users/story/3301492)

[![Dependency Status](https://www.versioneye.com/user/projects/5a72ff0b0fb24f5f2931a3f4/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5a72ff0b0fb24f5f2931a3f4)